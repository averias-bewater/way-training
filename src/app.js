/**
 * This file is part of the cis-server project.
 *
 * @project    way-server
 * @author     Rafa Campoy <rafa.campoy@gmail.com>
 * @copyright  2015 - Rafael Campoy
 * Licensed under the EUPL, Version 1.1 or – as soon they will be
 * approved by the European Commission – subsequent versions of the
 * EUPL (the "Licence");* you may not use this work except in
 * compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://www.osor.eu/eupl/european-union-public-licence-eupl-v.1.1
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the Licence is distributed on an "AS
 * IS" BASIS, * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing permissions
 * and limitations under the Licence.
 */

"use strict";

var nconf = require('nconf');
/**
 * configure usage of args, env and other variables
 */
nconf.argv()
    .env()
    .file({ file: __dirname + '/config.json' });

require('./app/modules/training/controller');