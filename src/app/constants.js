/**
 * This file is part of the cis-server project.
 *
 * @project    way-server
 * @author     Rafa Campoy <rafa.campoy@gmail.com>
 * @copyright  2015 - Rafael Campoy
 * Licensed under the EUPL, Version 1.1 or – as soon they will be
 * approved by the European Commission – subsequent versions of the
 * EUPL (the "Licence");* you may not use this work except in
 * compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://www.osor.eu/eupl/european-union-public-licence-eupl-v.1.1
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the Licence is distributed on an "AS
 * IS" BASIS, * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing permissions
 * and limitations under the Licence.
 */
function define(name, value) {
    Object.defineProperty(exports, name, {
        value:      value,
        enumerable: true
    });
}

define("LOGIN_SUCCESS", 10);
define("LOGIN_ERROR", 20);
define("START_TRAINING", 30);
define("END_TRAINING", 40);
define("SAVE_TRAINING", 50);
define("COORDINATE", 60);
define("REQUEST_ADD_OBSERVER", 70);