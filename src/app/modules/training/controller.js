/**
 * This file is part of the cis-server project.
 *
 * @project    way-server
 * @author     Rafa Campoy <rafa.campoy@gmail.com>
 * @copyright  2015 - Rafael Campoy
 * Licensed under the EUPL, Version 1.1 or – as soon they will be
 * approved by the European Commission – subsequent versions of the
 * EUPL (the "Licence");* you may not use this work except in
 * compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://www.osor.eu/eupl/european-union-public-licence-eupl-v.1.1
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the Licence is distributed on an "AS
 * IS" BASIS, * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing permissions
 * and limitations under the Licence.
 */

"use strict";

/**
 *
 * Variables
 */
var trainingServer = require('./server').trainingServer,
    elasticsearch = require('elasticsearch'),
    wayServerClientController = require('./../way-server-client/controller'),
    websocketController = require('./../websocket/controller'),
    util = require('util'),
    nconf = require('nconf'),
    bodyParser = require('body-parser'),
    fs = require('fs'),
    redis = require('redis'),
    mConstants = require("./../../constants");

trainingServer.use(bodyParser.json()); // support json encoded bodies
trainingServer.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

/**
 *
 * ElasticSearch Client
 */
var esServer = nconf.get('elasticsearchServer');
var client = new elasticsearch.Client({
    host: esServer.host + ':' + esServer.port,
    log: 'error'
});

/** Users and Observers arrays */
var users = {};
var pending = {};
var userObservers = {};

/**
 *  Redis clients
 */
var redisClient = redis.createClient();
var pub = redis.createClient();

// Routes

/**
 * Check if ElasticSearch (ES) server is available
 */
trainingServer.get('/connection', function (req, res) {
    var connection = function (statusCode, message){
        res.status(statusCode).json(
            {
                statusCode: statusCode,
                message: message }
        );
    };

    /**
     * Check if ES server is up
     */
    client.ping({
        requestTimeout: 30000,
        maxRetries: 2,
        hello: "elasticsearch!"
    }).then(function (body) {
        util.log(body);
        connection(200, 'ES server available');
    }, function (error) {
        util.log(error.message);
        connection(503, 'ES server unavailable');
    });
});

/**
 * Start training
 */
trainingServer.post('/start', function(req, res) {
    var userId = req.body.user_id;
    var loginToken = req.body.token;
    var chooseObservers = req.body.choose_observers;
    var trainingToken = '';
    var indexName = 'user' + userId;
    var start = function (statusCode, message){
        res.status(statusCode).json(
            {
                statusCode: statusCode,
                message: message,
                token: trainingToken,
                userId: userId
            }
        );
    };

    /**
     * Check in way-server if user is authenticate and receive training token. After if first training user, create
     * user index in ES
     */
    wayServerClientController.to('/training/start', 'post', {
        token: loginToken,
        user_id: userId
    }).then(
        function success(response) {
            trainingToken = response.training_token;

            util.log('*** Training token received from way-training for user ' + userId);

            client.indices.exists({
                index: indexName
            }).then(function (body) {
                if (!body) {
                    util.log('*** ES: Index ' + indexName + ' does not exist. Creating it...');
                    client.indices.create({ index: indexName }, function(err, body, code) {
                        if (err) {
                            start(500, 'index ' + indexName + ' could not be created');
                            throw new Error(err)
                        }
                        var mapping = JSON.parse(fs.readFileSync(__dirname + '/../../../training.json', 'utf8'));
                        client.indices.putMapping({ index: indexName, type: 'training', body: mapping['mappings'] }, function(err, body, code) {
                            if (err) {
                                start(500, 'index ' + indexName + ' could not be created');
                                throw new Error(err)
                            }
                        });
                    });
                }
                /**
                 *
                 * Add user and its observers in memory
                 */
                users[userId] = trainingToken;
                userObservers[userId] = chooseObservers;

                /**
                 * Send to active observers start training message
                 */
                websocketController.toActiveObservers(
                    chooseObservers,
                    userId,
                    'message',
                    JSON.stringify(
                        {
                            type: mConstants.START_TRAINING,
                            training_token: trainingToken
                        }
                    )
                );

                /**
                 * Create redis channels names = 'trainingToken.USER/id'. We save them in a Redis Hash with key = OBSCH/USER/userID
                 * and field = observer_id
                 */

                if (typeof userObservers[userId] !== 'undefined') {
                    userObservers[userId].forEach(function (observer) {
                        // Create a set with key 'OBSCH/User/'+userId' giving a channel for each chosen
                        // observer. Channel name is like 'trainingToken+'.USER/'+observerId'
                        redisClient.hset('OBSCH/User/'+userId, observer, trainingToken+'.USER/'+observer);
                        //set ttl for this key to 10 hours
                        redisClient.expire('OBSCH/User/'+userId, 36000);
                    });
                }

                start(200, 'index ' + indexName + ' was created successfully');
            }, function (error) {
                util.log('error: ' + error.message);
                start(500, error.message);
            });
        },
        function error(body) {
            util.log('*** Error retrieving training token from way-training for user ' + userId);
            start(500, 'Error retrieving training token for user ' + userId);
        }
    );
});

/**
 * Save a location coordinate in ES and Redis and publish on each observer channel
 */
trainingServer.post('/coordinate', function(req, res) {

    var location = req.body.location;
    var userId = location.user_id;
    var indexName = 'user' + userId;
    var trainingToken = location.token;

    var coordinate = function (statusCode, message){
        res.status(statusCode).json(
            {
                statusCode: statusCode,
                id: location.id,
                message: message
            }
        );
    };

    if (typeof users[userId] !== 'undefined') {
        if (users[userId] !== trainingToken) {
            coordinate(401, 'invalid token');
            return;
        }

        // save location
        client.create({
            index: indexName,
            type: 'training',
            body: location
        }).then(function (body) {

            /**
             * Add in a Redis Sorted Set each new coordinate
             */
            redisClient.zadd(trainingToken, location.id, JSON.stringify(location));
            redisClient.expire(trainingToken, 3600);
            redisClient.zrange(location.token, 0, -1, function(err, reply) {
                if (err) throw err;
            });

            /**
             * Publish each new coordinate in all redis user channels
             */
            userObservers[userId].forEach(function (observer) {
                redisClient.hget('OBSCH/User/'+userId, observer, function(err, reply) {
                    if (err) throw err;
                    pub.publish(reply, JSON.stringify(req.body));
                });
            });

            coordinate(201, 'location saved');

        }, function (error) {
            util.log(error.message);
            coordinate(503, 'Server unavailable');
        });

    } else {
        // does not exist
        util.log('*** User ' + userId + ' does not exist');
        coordinate(401, 'user does not authenticate')
    }
});

/**
 * End training and send observers end training message
 */
trainingServer.post('/end', function(req, res) {
    var userId = req.body.user_id;
    var trainingToken = req.body.token;

    var end = function (statusCode, message){
        res.status(statusCode).json(
            {
                statusCode: statusCode,
                message: message
            }
        );
    };

    if(typeof users[userId] === 'undefined' ||  users[userId] !== trainingToken) {
        end(404, 'user has no training started or wrong token');
        util.log('*** User ' + userId + ' has no training started or wrong token');
        return;
    }

    wayServerClientController.to('/training/end', 'post', {
        training_token: trainingToken,
        user_id: userId
    }).then(
        function success(response) {
            pending[userId] = users[userId];
            delete users[userId];

            /**
             * Send to active observers for current user end training message
             */
            websocketController.toActiveObservers(
                userObservers[userId],
                userId,
                'message',
                JSON.stringify(
                    {
                        type: mConstants.END_TRAINING,
                        training_token: trainingToken
                    }
                )
            );

            end(200, 'training ended');
            util.log('*** Training ended for user ' + userId);
        },
        function error(body) {
            util.log('*** Training for user ' + userId + ' could not be ended');
            end(500, 'training for user ' + userId + ' could not be ended');
        }
    );

});

/**
 * Saved or deleted training based on user choice
 */

trainingServer.post('/save', function(req, res) {
    var userId = req.body.user_id;
    var trainingToken = req.body.training_token;
    var save = req.body.save;
    var resume = req.body.resume;
    var coordinates = req.body.coordinates;

    var saveJSONResponse = function (statusCode, message){
        res.status(statusCode).json(
            {
                statusCode: statusCode,
                message: message
            }
        );
    };
    // first check if training is pending on saving

    if(typeof pending[userId] === 'undefined' ||  pending[userId] !== trainingToken) {
        util.log('*** User has no pending training when saving');
        res.status(404).json(
            { message : 'user has no training pended on saving'}
        );
        return;
    }

    var data = {};
    data['training_token'] = trainingToken;
    data['user_id'] = userId;
    data['save'] = save;
    data['coordinates'] = coordinates;
    data['resume'] = resume;

    /**
     * Call way-server to save training data
     */
    wayServerClientController.to('/training/save', 'post', {
        data: data
    }).then(
        function success(response) {
            delete pending[userId];
            /**
             * Send to active observers for current user save training message with 'saved' true/false
             */
            websocketController.toActiveObservers(
                userObservers[userId],
                userId,
                'message',
                JSON.stringify(
                    {
                        type: mConstants.SAVE_TRAINING,
                        training_token: trainingToken,
                        saved: save,
                        training_id: response.training_id
                    }
                )
            );
            delete userObservers[userId];
            saveJSONResponse(200, 'training saved' + response.toString());
            util.log('*** Training saved for user ' + userId + ' register: ' + response.count);
        },
        function error(body) {
            delete userObservers[userId];
            util.log('*** Training for user ' + userId + ' could not be saved');
            body.toString();
            saveJSONResponse(500, 'training for user ' + userId + ' could not be saved');
        }
    );

    /**
     * Delete on Redis all information about training
     */

    redisClient.del('OBSCH/User/'+userId);
    redisClient.del(trainingToken);

});