/**
 * This file is part of the cis-server project.
 *
 * @project    way-server
 * @author     Rafa Campoy <rafa.campoy@gmail.com>
 * @copyright  2015 - Rafael Campoy
 * Licensed under the EUPL, Version 1.1 or – as soon they will be
 * approved by the European Commission – subsequent versions of the
 * EUPL (the "Licence");* you may not use this work except in
 * compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://www.osor.eu/eupl/european-union-public-licence-eupl-v.1.1
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the Licence is distributed on an "AS
 * IS" BASIS, * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing permissions
 * and limitations under the Licence.
 */
"use strict";

var io = require('./server').io,
    wayServerClientController = require('./../way-server-client/controller'),
    util = require('util'),
    redis = require('redis'),
    mConstants = require("./../../constants");;

/**
 * Users connected and socket
 */
var currentUsers = {};
var currentSockets = {};

/**
 * Send event to user
 * Further params will be broadcasted
 * @param {number} id
 * @param {string} event
 * @return {Object}
 */
var toUserFunction = function (id, event) {
    var args = Array.prototype.slice.call(arguments);
    args[0] = 'USER/' + id;
    toChannelFunction.apply(this, args);
    util.log('*** Sent message to ' + args[0] + ' channel');
    return this;
};

/**
 * Broadcast event
 * Further params will be broadcasted
 * @param {string} event
 * @return {Object}
 */
var toAllFunction = function (event) {
    io.emit.apply(io, Array.prototype.slice.call(arguments));
    util.log('*** Sent message to "ALL" channel');
    return this;
};


/**
 * Check which observers are connected and send message only connected
 * @param {array} chooseObservers
 * @param {number} id
 * @param {string} event
 * @return {Object}
 */
var toActiveObserversFunction = function (chooseObservers, id, event) {
    var args = Array.prototype.slice.call(arguments, 1);

    if (typeof chooseObservers !== 'undefined' && chooseObservers.length > 0) {
        chooseObservers.forEach(function (observer) {
            if(typeof currentUsers[id] !== 'undefined') {
                args[0] = observer;
                var JsonMessage = JSON.parse(args[2]);
                JsonMessage.user_full_name = currentUsers[id].fullName;
                args[2] = JSON.stringify(JsonMessage);
                toUserFunction.apply(this, args);
            }
        });
    }
    return this;
};

/**
 * Send event to channel
 * Further params will be broadcasted
 * @param {string} channel
 * @param {string} event
 * @return {Object}
 */
var toChannelFunction =  function (channel, event) {
    var to = io.to(channel),
        args = Array.prototype.slice.call(arguments, 1);
    to.emit.apply(to, args);
    util.log('*** Sent message to ' + channel + ' channel with ' + JSON.stringify(args));
    return this;
};

/**
 * Handle connections
 */
io.on('connection', function (socket) {

    /**
     * Send a heartbeat every 15 seconds avoiding connection close
     */

    socket.on('pong', function(data){
        //util.log("Pong received from client");
    });
    setTimeout(sendHeartbeat, 15000);

    function sendHeartbeat(){
        setTimeout(sendHeartbeat, 15000);
        io.sockets.emit('ping', { beat : 1 });
    }

    /**
     * Authenticate given userId/token against way-server
     * and joins to all channel and user channel
     *
     * @param userId
     * @param token
     */
    function login(userId, token) {
        logout();

        /**
         *
         */
        wayServerClientController.to('/training/activeobservers', 'post', {
            token: token,
            user_id: userId
        }).then(
            function success(response) {
                var observers = response.observers;

                currentSockets[socket.id] = userId;
                socket.join('ALL');
                socket.join('USER/' + userId);

                currentUsers[userId] = {
                    token: token,
                    fullName: response.name + ' ' + response.surname,
                    observers: observers
                };

                socket.emit('message', JSON.stringify({type: mConstants.LOGIN_SUCCESS}));

                util.log('*** SOCKET: Sending login/success to User: ' + userId);
            },
            function error(body) {
                util.log(
                    'way-training error' +
                    '\nuser ' + userId +
                    '\ntoken: ' + token +
                    '\nresponse: ' + body
                );
                socket.emit('message', JSON.stringify({type: mConstants.LOGIN_ERROR}));
            }
        );
    }

    /**
     * Take user out from channels and delete all information
     */
    function logout() {
        var userId = currentSockets[socket.id];
        if(!userId) {
            return;
        }

        var rooms = socket.rooms;
        delete rooms[socket.id];

        rooms.forEach(function (room) {
            socket.leave(room);
        });

        delete currentSockets[socket.id];
        delete currentUsers[userId];

    }

    /**
     * Observer is subscribed to a Redis channel after accepting to follow a training
     *
     * @param userId
     * @param trainingToken
     * @param accepted
     */
    function acceptTraining(userId, trainingToken, accepted) {
        if(accepted) {
            var sub = redis.createClient();

            sub.on("message", function (channel, message) {
                socket.emit(
                    'message',
                    JSON.stringify({
                        type: mConstants.COORDINATE,
                        content: message
                    })
                );
                util.log('*** SOCKET=> Sending COORDINATE  to User: ' + userId);
            });

            sub.on("subscribe", function (channel, count) {
                util.log('*** REDIS: User '+ userId +' have been subscribed to channel: ' + trainingToken + '.USER/' + userId)
            });

            sub.subscribe(trainingToken + '.USER/' + userId);

        } else {
            //TODO: delete Redis observer subscription channel
        }
    }

    /**
     * Send a message to users for accepting or rejecting to be observer
     * @param userId
     * @param observerId
     */
    function askAddObserver(userId, observerId) {

        if(typeof currentUsers[userId] !== 'undefined') {
            util.log('user: ' + userId + ' observer: ' + observerId);
            toUserFunction(
                observerId,
                "message",
                JSON.stringify(
                    {
                        type: mConstants.REQUEST_ADD_OBSERVER,
                        user_id: userId,
                        full_name: currentUsers[userId].fullName

                    }
                )
            );
            util.log('*** Sent request to Add Observer to observer id = ' + observerId + ' from User ' + currentUsers[userId].fullName + ' with user id = ' + userId);
        } else {
            util.log('Weird... but user = ' + userId + ' was not connected')
        }
    }


    socket.on('user/login', login);
    socket.on('user/logout', logout);
    socket.on('disconnect', logout);
    socket.on('accept/training', acceptTraining)
    socket.on('request/add/observer', askAddObserver)
});



/**
 * Clients communication module
 * @type {Object}
 */
module.exports = {
    toAll: toAllFunction,
    toActiveObservers: toActiveObserversFunction,
    toUser: toUserFunction,
    toChannel: toChannelFunction
};