/**
 * This file is part of the cis-server project.
 *
 * @project    way-server
 * @author     Rafa Campoy <rafa.campoy@gmail.com>
 * @copyright  2015 - Rafael Campoy
 * Licensed under the EUPL, Version 1.1 or – as soon they will be
 * approved by the European Commission – subsequent versions of the
 * EUPL (the "Licence");* you may not use this work except in
 * compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://www.osor.eu/eupl/european-union-public-licence-eupl-v.1.1
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the Licence is distributed on an "AS
 * IS" BASIS, * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing permissions
 * and limitations under the Licence.
 */

"use strict";

var querystring = require('querystring'),
    Q = require('q'),
    nconf = require('nconf'),
    options = nconf.get('wayServer'),
    http = require(options.ssl === true ? 'https' : 'http');

/**
 * way-server communication module
 * @type {Object}
 */
module.exports = {
    /**
     * Send data to way-server
     *
     * @param {string} url
     * @param {string} method
     * @param {*} [data]
     * @returns {Q.promise}
     */
    to: function (url, method, data) {
        var deferred = Q.defer(),
            body = '',
            req;

        method = String(method).trim().toUpperCase();
        if (method == 'GET') {
            url += '?' + querystring.stringify(data);
        } else {
            body = JSON.stringify(data);
            options.headers['Content-Length'] = body.length;
        }
        options.path = url;
        options.method = method;
        req = http.request(options, function (res) {
            var body = '';
            res.setEncoding('utf-8');
            res.on('data', function (data) {
                body += data;
            });
            res.on('end', function () {
                if(!(/^2\d\d$/).test(res.statusCode)) {
                    deferred.reject(body);
                    return;
                }
                deferred.resolve(JSON.parse(body));
            });
            res.on('error', function(e) {
                deferred.reject(res);
            });
        });

        req.on('error', function(e){
            deferred.reject(e);
        });
        req.end(body);

        deferred.promise.abort = req.abort.bind(req);
        return deferred.promise;
    }
};